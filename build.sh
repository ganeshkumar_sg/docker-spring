mvn clean install

docker build --no-cache -t ganesh/rest-controller-inside-docker:1.0 .

docker run --rm -d -p 127.0.0.1:5000:5000 --name=rest-controller-inside-docker --net=host ganesh/rest-controller-inside-docker:1.0