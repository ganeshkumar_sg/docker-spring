FROM openjdk:8u111-jdk

ADD target/docker-spring-0.0.1-SNAPSHOT.jar docker-spring-0.0.1-SNAPSHOT.jar

EXPOSE 5000

CMD java -jar docker-spring-0.0.1-SNAPSHOT.jar