package com.senecaglobal.learn.dockerspring.repository;


import java.util.List;

import com.senecaglobal.learn.dockerspring.entity.Customer;
import org.springframework.data.repository.CrudRepository;

public interface CustomerRepository extends CrudRepository<Customer, Long> {

    List<Customer> findByLastName(String lastName);
}