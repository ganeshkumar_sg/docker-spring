package com.senecaglobal.learn.dockerspring.controller;

import java.util.ArrayList;
import java.util.List;

import com.senecaglobal.learn.dockerspring.entity.Customer;
import com.senecaglobal.learn.dockerspring.model.CustomerModel;
import com.senecaglobal.learn.dockerspring.repository.CustomerRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CustomerController {

    @Autowired
    private CustomerRepository customerRepository;

    @RequestMapping(value = "/customer", method = RequestMethod.GET)
    public List<CustomerModel> getCustomer() {
        List<CustomerModel> customers = new ArrayList<>();

        customerRepository.findAll().forEach(customer -> {
            CustomerModel theCustomerModel = new CustomerModel();
            BeanUtils.copyProperties(customer, theCustomerModel);
            customers.add(theCustomerModel);
        });

        return customers;
    }

    @RequestMapping(value = "/customer", method = RequestMethod.POST)
    public void addCustomer(@RequestBody CustomerModel customerModel) {
        Customer customer = new Customer();
        BeanUtils.copyProperties(customerModel, customer);
        customerRepository.save(customer);
    }
}
