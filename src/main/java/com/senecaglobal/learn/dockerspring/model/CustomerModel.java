package com.senecaglobal.learn.dockerspring.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@ToString
public class CustomerModel {

    private String firstName;
    private String lastName;
    private Long id;
    private String email;
    private Double salary;
}
